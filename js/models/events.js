function eventCtrl($scope){
    choice = function(text, action, destination) {
        this.text = text;
        this.action = action;
        this.destination = destination;
    };
    eventCard = function(text, chos) {
        this.eventText = text;
        this.choices = chos;
    };
    $scope.bestiary = {
        SlateImp: {
            name: "slate imp",
            stats: {
                str : 1,
                def : 2,
                "int" : 1,
                spc : 1,
                luk : 1,
                hp  : 10,
                lus : 0
            },
            intro: "<p>A Slate Imp appears!</p>",
            attacks: [{hit: {text: "<p>He hits you!</p>", effect: function(){character.hp -= 1;}}}],
            win: "<p>You Win!</p>",
            lose: "<p>You lost and probably got raped.</p>",
            loot: {
                low: 1,
                high: 10,
                items: ["GenericItem"]
            }
        }
    };
    $scope.foe = function(){
        return $scope.bestiary["SlateImp"]
    };
    $scope.enemy = null;
    $scope.curEvent = "title";
    $scope.events = {
        title: new eventCard(
            "<p>\
                <b>Welcome to CoCStuck 0.0.1</b>: an XLR joint.<br />\
                <u>DISCLAIMER:</u><br />\
                <i>Fetishes abound! Play at your own risk.<br />\
                Goes without saying, but you should be at or above the legal\
                age to view porn in your country to play this game.</i>\
            </p>",
            null
        ),
        newGame: new eventCard(
            "<p>\
                A person of as of yet undefined gender sits on a bench. It\
            turns out that today is the start of this person's vaction. Though\
            it was many years ago he or she was given life, it is only today\
            he or she will be given a name!\
            </p>",
            [new choice("Enter Name.",null,"name")]
        ),
        name: new eventCard(
            "<p>\
                <input type='text' ng-model='character.name' /><label for='name' id='nameLabel'>Enter your name</label>\
            </p>",
            [new choice("==>",null,"sex")]
        ),
        sex: new eventCard(
            "<p>\
                The unmarked building across the street is similar to most of\
            the other buildings downtown, save for the complete lack of people\
            streaming in and out of it. In fact, as you watch the people\
            walking by it from the bench, you swear they are activlely trying\
            to avoid looking at it. You glance at your watch. The bus that you\
            are taking to the airport should be coming any minute now. \
            </p>\
            <p>\
                \"It's a cellular antenna,\" a voice breaks your concentration.\
            The owner of the voice, a tall man with doe eyes hidden behind\
            frameless glasses and a scraggly beard that draws attention to his\
            puffy lips, stares right at you. He sits down next to you and\
            continues. \"They're pretty ugly, but businesspeople need cellular\
            coverage, so they just cover them up with building stuff.\" \
            </p>\
            <p>\
                You are not sure how to address this stranger. You gather your\
            belongings, a wheeled suitcase and a backpack, and start to stand\
            up, when he extends a hand. \"They call me Andrew,\" he says. His\
            voice is really soothing. You shake his hand and introduce\
            yourself. He blushes a little. \"Um, sorry, but are you a guy or a\
            girl? I can\'t tell.\"\
            </p>",
            [
                new choice("Boy.",{sex: "male"},"intro"),
                new choice("Girl.",{sex :"female"},"intro")
            ]
        ),
        intro: new eventCard(
            "<p ng-init='character.sex = sex'>\
                He chuckles lightly, allowing the sun to hit his hairline,\
            highlighting his closely cropped brown hair. \"So\
            {{character.name}}, do you want to go inside? I'm sure a\
            strapping young {{character.sex}} would be interested in\
            getting to see this up close and personal.\"\
            </p>\
            <p>\
                You are a little confused as to what he's talking about.\
            </p>", 
            [
                new choice("Yes.", {hussChoice: true},"weight"),
                new choice("No.", {hussChoice:false}, "weight")
            ]
        ),
        weight: new eventCard(
            "<p ng-switch on='hussChoice'>\
                <span ng-switch-when='false'>\
                    Your brain is screaming no, but Andrew's voice is so\
            hypnotic that your body can't help but follow him across the\
            street to the antenna building. Up close, you notice that you\
            can't see inside the building.\
                </span>\
                <span ng-switch-default>\
                    Andrew's voice is so hypnotic that you can't help but\
            follow him across the street to the antenna building. Up close,\
            you notice that you can't see inside the building.\
                </span>\
            </p>\
            <p>\
                As you realize that no one will be able to notice you inside, Andrew flings open the doors closest to you and pushes you inside, throwing your bags in after you, and then closes the doors.\
            </p>\
            <p>\
                How much did it hurt?\
            </p>",
            [
                new choice("A Lot!", {
                    weight:{
                        wei: 1,
                        desc: "skinny",
                        build: "light"
                    }
                }, "introHouse"), 
                new choice("Sort of...", {
                    weight:{
                        wei: 3,
                        desc: "avg",
                        build: "average"
                    }
                }, "introHouse"),
                new choice("Only my ego.",{
                    weight:{
                        wei: 5,
                        desc: "fat",
                        build: "heavy"
                    }
                }, "introHouse")
            ]
        ),
        introHouse: new eventCard(
            "<p ng-init='jQuery.extend(true, $parent.character, $scope.weight)'>\
                It is pitch dark. \
            </p>\
            <p>\
                You dust yourself off, your $build bearing the brunt of the\
            fall. After a few minutes of groping around, you find a light\
            switch. The lights flicker as they turn on. You are shocked by\
            what you see.\
            </p>\
            <p>\
                It is a studio apartment. There is an old TV (not working)\
            with a futon in front of it, currently set up so it can serve as\
            a couch. Sunbeams from a nearby window hit the futon, making\
            sleeping during the day difficult. In a corner, there is a\
            kitchenette, with a fridge and sink. There is a stove, but it\
            appears to be an early electric model, and does not have a\
            matching oven. The walls are painted in a felt green. Behind you,\
            the door you came in is no longer the wide swinging doors of the\
            antenna building, but the sliding metal doors of a freight\
            elevator. Next to the light switch that you just flipped, there is\
            a button that appears to call the elevator.\
            </p>\
            <p>\
                Most of the surfaces are covered with dust. It seems as if no\
            one has lived here for a long time. Unsurprisingly, your cellphone\
            is not working, but your watch is.\
            </p>",
            [new choice("==>",null,"hub")]
        ),
        hub: new eventCard(
            "<p ng-init='$parent.character.activateCharacter()'>\
                The apartment is a tight fit, but it is yours for the time\
            being. It hasn't been used for a long time, storing up potential\
            energy that expresses itself in small static shocks when you walk\
            around. It is your eye in the storm of unpredictability that is\
            your life now.\
            </p>",
            [new choice("Leave.", null, "lift")]
        ),
        lift:new eventCard(
            "<p>\
                The door of the lift slides open. You walk in and the door\
            slides shut as you notice the elevator's lack of buttons.\
            </p>\
            <p>\
                The elevator hums for a while and opens up for you to see...\
            </p>",
            [new choice("==>", null, "sForestI")]
        ),
        sForestI:new eventCard(
            "<p>\
                A dark forest. As you make your way through the forest, you\
            see an opening ahead. Stepping into a small clearing, you find\
            that the sun is beginning to set over the horizon, with night soon\
            to follow. As you begin your way home, you glimpse a faint,\
            yellow light in the distance, making faint, erratic movements. Do\
            you investigate the strange light?\
            </p>",
            [
                new choice("Yes", null, "sForestIIY"),
                new choice("No", null, "sForestIIN")
            ]
        ),
        sForestIIN: new eventCard(
            "<p>\
                You forget about the light and continue walking until you find\
            your way back to the elevator door as night sets in.\
            </p>",
            [new choice("==>", null,"hub")]
        ),
        sForestIIY: new eventCard(
            "<p>\
                Curious about such a site, you find yourself dismissing the\
            urgency to return to camp, and head out towards the underbrush\
            where you saw the strange anomaly. The light seems to glow\
            brighter and brighter as you make your way ever so closer, with a\
            faint hum reverberating in the air as well. Finding yourself a few\
            yards in front of the glow, you crouch down and began to peel open\
            a looking hole in the bush.  Before glimpsing anything however, a\
            hand darts straight through the shrub, grasping your neck with\
            great ferocity! Shocked, the hand pulls you through the bush\
            straight into the air, the figure before you emitting the light\
            you saw before. However, before even getting a glimpse of it, you\
            humbly faint and go unconscious.\
            </p>\
            <p>\
                You come to several hours later, waking up at around noon.\
            Finding yourself in a camp for some reason and rubbing your sore\
            neck while looking around at your surroundings, it suddenly makes\
            itself apparent, you aren’t alone. There, right in front of you,\
            is standing an immense beauty. A great woman, with glistening\
            black lips, emerald green eyes and luscious black hair, coupled\
            with flawless soft-beige skin. She is clothed in a stunningly\
            beautiful reddish bodysuit, emphasizing every curve on her frame,\
            with some kind of Chinese symbols written across her perky\
            breasts. Two sets of powerful wings flutter behind her, easily\
            capable of quick flight. Thick arms. Thick legs. Dainty fingers.\
            As your eyes lower to take in all of her body, you notice\
            something peculiar: her butt. Somehow noticing your curiosity in\
            her derrière, she turns to the side on her black heels and sticks\
            out her tush. Her rump is enormous. Through her bodysuit you can\
            easily make out her large, supple ass and extra-wide hips,\
            emphasizing her large but cheeks, the cleavage between her cheeks\
            taunting your lust, as you feel yourself drool quite a bit at the\
            view .Even more amazing is how her butt practically glows.\
            Pulsating out a bright yellow light that seems to be connected to\
            her breathing, it feels tantalizing and calm to be within its\
            presence. Coming back to your sense, you look up at the firefly\
            girl, and ask her what she wants. She smirks a quick smile and\
            explains \"Well, I guess I should apologize for attacking your\
            earlier, thought you were an imp readying itself for an ambush.\"\
            </p>\
            <p>\
                \"...never can be too careful when you’re foraging for\
            supplies\" she mutters under her breath.\
            </p>\
            <p>\
                You make clear that you accept her apology, but ask who she\
            is, and why she is still here.\
            </p>\
            <p>\
                \"Oh, I guess I should have introduced myself at the start\
            huh. My name is Serenity, and, as if you have already guessed, I\
            am a firefly girl. Very rare in these parts actually.\"\
            </p>\
            <p>\
                \"As for why I haven't left yet well...\" her lips moving into\
            a small lustful grin \"I think I owe you a favor for almost\
            killing you, don't you think?\"\
            </p>",
            [
                new choice("Take the favor.", null, "sForestIIIY"),
                new choice("Run.", null, "sForestIIIN")
            ]
        ),
        sForestIIIN: new eventCard(
            "<p>\
                You quickly get to your feet and sprint away, waiting behind a\
            tree for a few hours until you are sure she has flown away before\
            making your way back to the elevator.\
            </p>"
            [new choice("==>", null, "hub")]
        ),
        sForestIIIY: new eventCard(
            "<p>\
                Before you can say anything, she has already stripped both you\
            AND herself naked! Her wings must be WAY faster than you thought.\
            </p>\
            <p>\
                Letting out a shaky breath as Serenity lays on the ground in\
            front of you, she gazes over your eight inch prick, standing at\
            full attention in front of her. Creeping ever so closer, she\
            pressed her face straight into your crotch, inhaling the smell\
            deeply as kisses your balls gently, raising her head up slowly as\
            she runs her tongue up along your shaft, her mouth open as she\
            reaches the top and begins to suckle at the tip. The feel is\
            amazing; her tongue runs all over your cock, pressing and nudging\
            against the sensitive flesh in a clockwise motion, her eyes are\
            closed as she revels in sucking your manhood. Abruptly however,\
            she stops, pulling her mouth away from your prick with a slimy\
            string of saliva trailing behind. Wiping her mouth for a second,\
            she quickly does a 180 degree turn, and sticks out her huge behind\
            right in front of your face. The glow is even brighter now, either\
            due to her bum being free of clothing or from tension, but you can\
            still make out her dripping pussy and tight asshole.\
            </p>\
            <p>\
                \"Well now, what'll it be then?\"\
            </p>",
            [
              new choice("Ass.",{sFSC: "ass"}, "sForestIV"),
              new choice("Pussy.", {sFSC: "pussy"}, "sForestIV")
            ]
        ),
        sForestIV: new eventCard(
            "<div ng-switch on='sFSC'>\
                <div ng-switch-when='ass'>\
                    <p>\
                        You mumble a bit under bated breath \"Y-your ass\
            pp-please\"\
                    </p>\
                    <p>\
                        She bites her lip a bit in anticipation. \"Perfect.\"\
                    </p>\
                    <p>\
                        Situating herself so that she is facing you, she\
            begins to squat so that your cock is right below her awaiting\
            butthole. Widening her stance a bit, she slowly lowers her bum\
            until the tip of your prick is nudging against her rear entrance.\
            She prods her bum against your cock several times, adjusting\
            herself several times, before eventually pressing slowly but with\
            great strength, her asshole letting out a quiet \"shquish\" as her\
            butt envelops your tip. Letting out a shrill moan, she starts\
            pumping against your cock. With each thrust she manages to get\
            more and more of your shaft inside her. Her tight anus squeezes\
            against your dick, rubbing against it with enormous pleasure. With\
            each bob up and down, her breasts jiggle a bit, her light brown\
            nipples shaking erratically as her copious assflesh rubs all\
            around your prick, with her glowing ass pulsating wildly, as if it\
            was a strobe light. Managing to get all eight inches inside her,\
            Serenity began sliding long, slow drags along your shaft. She is\
            obviously enjoying this, with glazed eyes and her tongue hanging\
            on the side of her mouth, one hand playing with her cute breasts\
            while the other one pressed against the ground for leverage, and\
            beads of sweat dripping off her flawless skin.\
                    </p>\
                    <p>\
                        You can feel yourself coming close. With each slow\
            thrust her ass slid against your cock, you can feel the tension\
            grow greater and greater. Gripping her corpulent thighs while she\
            is at the top of the thrust, you press her down and insert all\
            eight inches inside her, all the way to the base, as you let out\
            your thick, copious load inside her. With the vicious cream\
            pooling inside her, Serenity herself began to orgasm, letting out\
            a quick moan series of hoarse breaths while her rear end begins\
            glowing brighter than you’ve ever seen.\
                    </p>\
                    <p>\
                        Feeling the need to take your leave, you begin to\
            slowly pull\ her bright rump off of you, but are stopped.\
                    </p>\
                    <p>\
                        \"No no….k-keep it in for now\" Serenity mutters,\
            still breathing deeply, as she begins to position both you and\
            herself lying down.\
                    </p>\
                    <p>\
                        \"Just keep it i-in\" she says quietly as she drifts\
            off to sleep, your member still plugging up the torrent of jizz\
            inside her ass.\
                    </p>\
                </div>\
                <div ng-switch-default>\
                    <p>\
                        Sorry, not implemented yet.\
                    </p>\
                </div>\
            </div>\
            <p>\
                You find yourself tired as well, and fade off into sleep.\
            </p>\
            <p>\
                You come to an hour or so later, still in your camp, but with\
            a kiss in black lipstick across your forehead.\
            </p>\
            <p>\
                There is a trail of what appears to be semen leading from your\
            crotch away from the clearing. You hope that you meet that\
            firefly lady again sometime soon.\
            </p>",
            [new choice("==>", null, "hub")]
        ),
        combatTestIntro: new eventCard(
            "<p ng-init='$parent.enemy = foe()' compile='$parent.enemy.intro'></p>",
            [new choice("end of demo", null, "title")]
        ),
        save: new eventCard(
            "<p>Save your game using the slots below</p>", []
        )
    };
    $scope.startNewGame = function(){
        $scope.curEvent = "newGame";
    };
    $scope.goEvent = function(choice){
        $scope.curEvent = choice.destination;
        $.extend(true, $scope, choice.action);
    }
    $scope.character = {
        active: false,
        stats: {
            name: { 
                fName: "Name",
                val: "",
                hidden: true
            },
            str : {
                fName: "Strength",
                val: 0,
                hidden: false
            },
            def : {
                fName: "Defense",
                val: 0,
                hidden: false
            },
            "int" : {
                fName: "Intelligence",
                val: 0,
                hidden: false
            },
            spc : {
                fName: "Special",
                val: 0,
                hidden: false
            },
            luk: {
                fName: "Luck",
                val: 0,
                hidden: false
            },
            hp: {
                fName: "Health",
                val: 0,
                hidden: false
            },
            lus : {
                fName: "Lust",
                val: 0,
                hidden: false
            },
            sex : {
                fName: "Sex",
                val: "",
                hidden: true
            },
            wei : {
                fName: "Weight",
                val: 0,
                hidden: true
            },
            weight: {
                val: "",
                hidden: true
            },
            build: {
                val: "",
                hidden: true
            }
        },
        change: function(stat, value){
            $scope.stats[stat] = value;
        }
    };
    $scope.character.activateCharacter = function(){
        $scope.character.active = true;
    }
    $scope.character.isActive = function(){
        return $scope.character.active;
    }
}
