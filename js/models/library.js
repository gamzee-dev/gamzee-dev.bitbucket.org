//Library of game variables.
//Library of events, here be spaghetti

//Library of descriptions
	var descriptions = {
		male: ["boy",  "man", "guy", "dude"],
		female: ["girl", "lady", "chick", "lass"],
		skinny: ["thin", "skinny", "compact", "waifish"],
		average: ["average","unremarkable","nondescript"],
		fat: ["heavy","fat","chunky","husky"],
		light: ["thin frame", "frail figure"],
		avg: ["average frame","typical features"],
		heavy: ["bulbous rolls of fat", "giant ass", "plush rump"]
	}
//Combat areas
	var areas = {
		test: ["SlateImp"]
	}
//Bestiary
	var bestiary = {
		SlateImp: {
			name: "slate imp",
			stats: {
				str : 1,
				def : 2,
				"int" : 1,
				spc : 1,
				luk : 1,
				hp  : 10,
				lus : 0
			},
			intro: "<p>A Slate Imp appears!</p>",
			attacks: [{hit: {text: "<p>He hits you!</p>", effect: function(){character.hp -= 1;}}}],
			win: "<p>You Win!</p>",
			lose: "<p>You lost and probably got raped.</p>",
			loot: {
				low: 1,
				high: 10,
				items: ["GenericItem"]
			}
		}
	}
